On accessibility SetSelectionRange, scroll the selection into view

When a blind user reads around in a page, he wants firefox to keep
showing on the screen the current caret, so that sighted colleagues can
see what he is moving to.  If the caret goes off what is visible, we
need to scroll to get the selection into view.

For normal keypresses this already works,
nsPhysicalSelectMoveScrollCommand::DoCommand calls
nsFrameSelection::MoveCaret which calls ScrollIntoView, before DoCommand
calls AdjustFocusAfterCaretMove(). HyperTextAccessible needs to do the
same to get the same behavior.

--- a/accessible/generic/HyperTextAccessible.cpp
+++ b/accessible/generic/HyperTextAccessible.cpp
@@ -1358,6 +1358,13 @@ HyperTextAccessible::SetSelectionRange(i
     domSel->RemoveRange(domSel->GetRangeAt(idx));
   SetSelectionBoundsAt(0, aStartPos, aEndPos);
 
+  // Make sure it is visible
+  domSel->ScrollIntoView(nsISelectionController::SELECTION_FOCUS_REGION,
+                         nsIPresShell::ScrollAxis(),
+                         nsIPresShell::ScrollAxis(),
+                         dom::Selection::SCROLL_FOR_CARET_MOVE |
+                             dom::Selection::SCROLL_OVERFLOW_HIDDEN);
+
   // When selection is done, move the focus to the selection if accessible is
   // not focusable. That happens when selection is set within hypertext
   // accessible.

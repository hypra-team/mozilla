Fix tab focus when tabbing through an element that refuses focus

Fix pending commiting

# HG changeset patch
# User Neil Deakin <neil@mozilla.com>
# Parent  76376169669fbc390d1cd297d4a3412f15f68461
Bug 422981, if the focused element is clear after focusing an element, still update the caret position. Additional fix when shift-tabbing from a caret position inside a focusable element to skip over that outer element.

diff --git a/dom/base/nsFocusManager.cpp b/dom/base/nsFocusManager.cpp
--- a/dom/base/nsFocusManager.cpp
+++ b/dom/base/nsFocusManager.cpp
@@ -2019,9 +2019,12 @@ nsFocusManager::Focus(nsPIDOMWindowOuter
   // needed. If this is a different document than was focused before, also
   // update the caret's visibility. If this is the same document, the caret
   // visibility should be the same as before so there is no need to update it.
-  if (mFocusedContent == aContent)
+  // As a special case when there is no focused element (implying that the
+  // element was likely blurred during focus), still update the caret position.
+  if (!mFocusedContent || mFocusedContent == aContent) {
     UpdateCaret(aFocusChanged && !(aFlags & FLAG_BYMOUSE), aIsNewDocument,
-                mFocusedContent);
+                aContent);
+  }
 
   if (clearFirstFocusEvent) mFirstFocusEvent = nullptr;
 }
@@ -2810,6 +2813,21 @@ nsFocusManager::DetermineElementToMoveFo
         nsCOMPtr<nsIContent> endSelectionContent;
         GetSelectionLocation(doc, presShell, getter_AddRefs(startContent),
                              getter_AddRefs(endSelectionContent));
+
+        // If we are going backwards from the caret point, locate an enclosing
+        // focusable element and use that instead.
+        if (startContent && !forward && !forDocumentNavigation) {
+          nsIContent* focusableContent = startContent;
+          while (focusableContent) {
+            if (focusableContent->GetPrimaryFrame() &&
+                focusableContent->GetPrimaryFrame()->IsFocusable()) {
+              startContent = focusableContent;
+              break;
+            }
+            focusableContent = focusableContent->GetParent();
+          }
+        }
+
         // If the selection is on the rootContent, then there is no selection
         if (startContent == rootContent) {
           startContent = nullptr;
